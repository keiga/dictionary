MTGに関する辞書ツールです。

|file|discription|
|:-|:-|
|card.txt|[カード名変換辞書データ](http://whisper.wisdom-guild.net/apps/autodic/)をUSキーボードに対応させたものです。容量が大きいので気をつけてください。|
|name.txt|MTG界でなかなか一発で変換できない人名をまとめてあります。カバレージを書くときなどにお使いください。|
